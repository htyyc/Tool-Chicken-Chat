1. 聊天类型增加未领取和已领取状态, 增加红包表
id
user_id
type
status 0有效 1抢完 2过期
price
create_time
update_time

2. 增加领取记录表，并增加手气最佳字段
id
red_package_id
price
status 0 普通 1 手气最佳
create_time
update_time

3. 增加用户余额
id
user_id
balance
update_time

4. 发起红包前先检查余额，并设置48小时后退还给用户（这个是放在redis做延迟处理还是定时任务处理？）
启动后台任务，每30分钟处理一次
5. 群指定个数红包随机金额以及定金额
6. 私聊发送红包
7. 后台充值提现功能开发（待定）